from rest_framework import serializers
from airport.models import Airport

class AirportSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Airport model """
    class Meta:
        model = Airport
        fields = ("Airport_ID", "Name", "IATA", "ICAO", "City", "Latitude", "Longitude")