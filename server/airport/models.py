from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator

class Airport(models.Model):
    id = models.AutoField(primary_key=True)
    Airport_ID = models.CharField(max_length=50)
    Name = models.CharField(max_length=150)
    City = models.CharField(max_length=150)
    Country = models.CharField(max_length=150)
    IATA = models.CharField(max_length=25)
    ICAO = models.CharField(max_length=25)
    Latitude = models.CharField(max_length=250)
    Longitude = models.CharField(max_length=250)
    Altitude = models.CharField(max_length=250)
    Timezone = models.CharField(max_length=100)
    DST = models.CharField(max_length=100)
    Tz = models.CharField(max_length=100)
    Type = models.CharField(max_length=100)
    Source = models.CharField(max_length=100)