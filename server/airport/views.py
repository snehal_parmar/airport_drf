from rest_framework import viewsets
from airport.models import Airport
from airport.serializers import AirportSerializer


class AirportViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Airport objects """
    queryset = Airport.objects.all()
    serializer_class = AirportSerializer
    def get_queryset(self):
    	iata_search = self.request.query_params.get('iata')
        airport_data = Airport.objects.filter(IATA__startswith=iata_search)
        return airport_data

class AirportNameViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Airport objects """
    serializer_class = AirportSerializer
    def get_queryset(self):
    	iata_search = self.request.query_params.get('airport_name')
        airport_data = Airport.objects.filter(Name__startswith=iata_search)
        return airport_data
