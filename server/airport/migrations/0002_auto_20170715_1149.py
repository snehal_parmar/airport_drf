# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airport', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='airport',
            old_name='NAME',
            new_name='Name',
        ),
    ]
