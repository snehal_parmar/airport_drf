# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Airport',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('Airport_ID', models.CharField(max_length=50)),
                ('NAME', models.CharField(max_length=150)),
                ('City', models.CharField(max_length=150)),
                ('Country', models.CharField(max_length=150)),
                ('IATA', models.CharField(max_length=25)),
                ('ICAO', models.CharField(max_length=25)),
                ('Latitude', models.CharField(max_length=250)),
                ('Longitude', models.CharField(max_length=250)),
                ('Altitude', models.CharField(max_length=250)),
                ('Timezone', models.CharField(max_length=100)),
                ('DST', models.CharField(max_length=100)),
                ('Tz', models.CharField(max_length=100)),
                ('Type', models.CharField(max_length=100)),
                ('Source', models.CharField(max_length=100)),
            ],
        ),
    ]
