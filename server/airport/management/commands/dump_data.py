from django.core.management.base import BaseCommand

from airport.models import Airport
import csv

path = 'airports_other.csv'


class Command(BaseCommand):
    help = 'Does some magical work'

    def dump_data(self, path):
        with open(path) as f:
            reader = csv.reader(f)
            for row in reader:
                if row[4] == '\\N':
                    print "skipping the row as iata code is none, row: {}".format(row)
                else:
                    print "inserting : {}".format(row)
                    _, created = Airport.objects.get_or_create(
                        Airport_ID = row[0],
                        Name = row[1],
                        City = row[2],
                        Country = row[3],
                        IATA = row[4],
                        ICAO = row[5],
                        Latitude = row[6],
                        Longitude = row[7],
                        Altitude = row[8],
                        Timezone = row[9],
                        DST = row[10],
                        Tz = row[11],
                        Type = row[12],
                        Source = row[13]
                        )
    
    def handle(self, *args, **options):
        self.dump_data(path)
        print "done inserting the data into db"

